class HomeController {
  constructor($http) {
    this.name = "home";

    this.base_url = "http://private-36ae7-bootcamp3.apiary-mock.com"
    this.posts = []
    $http.get(this.base_url + "/questions").then((response) => {
      this.posts = response.data;
    }, (response) => {
      console.log(response)
    })
  }
}

export default HomeController;
