import template from './comment.html';
import controller from './comment.controller';

let postComponent = {
  bindings: {
    comment: '<'
  },
  template,
  controller
};

export default postComponent;
