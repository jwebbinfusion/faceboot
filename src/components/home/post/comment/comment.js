import angular from 'angular';
import uiRouter from 'angular-ui-router';
import commentComponent from './comment.component';

let commentModule = angular.module('comment', [
  uiRouter
])

.component('comment', commentComponent)

.name;

export default commentModule;
