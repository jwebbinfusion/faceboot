import dateFormat from 'dateformat'

class CommentController {
  constructor() {
    this.name = 'comment';
    this.datestr = ''
  }

  $onInit() {
    const comment_date = new Date(this.comment.published_at)
    this.datestr = dateFormat(comment_date, "mmmm dS 'at' h:mmtt")
  }


}

export default CommentController;
