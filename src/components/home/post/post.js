import angular from 'angular';
import uiRouter from 'angular-ui-router';
import postComponent from './post.component';
import commentModule from './comment/comment';

let postModule = angular.module('post', [
  uiRouter,
  commentModule
])

.component('post', postComponent)

.name;

export default postModule;
