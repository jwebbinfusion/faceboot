import dateFormat from 'dateformat'

class PostController {
  constructor() {
    this.name = 'post';
    this.datestr = '';
    this.comment_input = '';
    this.liked = false;
  }

  $onInit() {
    const post_date = new Date(this.post.published_at)
    this.datestr = dateFormat(post_date, "mmmm dS 'at' h:mmtt")
  }

  clickLike() {
    if (this.liked) {
      this.post.likes--;
    } else {
      this.post.likes++;    
    }
    this.liked = !this.liked;
  }

  submitComment() {
    const comment = {
      username: 'Username',
      published_at: dateFormat(Date.now(), 'isoUtcDateTime'),
      content: this.comment_input
    };
    this.comment_input = '';
    this.post.replies.push(comment);
  }

}

export default PostController;
