import template from './post.html';
import controller from './post.controller';

let postComponent = {
  bindings: {
    post : '<'
  },
  template,
  controller
};

export default postComponent;
