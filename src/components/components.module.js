import angular from 'angular';
import Home from './home/home';

export const ComponentsModule = angular
  .module('app.components', [
    Home
  ])
  .name;
