import angular from 'angular';
import Nav from './nav/navbar';
import Sidebar from './sidebar/sidebar'

export const CommonsModule = angular
  .module('app.commons', [
    Nav,
    Sidebar
  ])
  .name;
