//require('./assets/styles/index.css');
require('./assets/styles/main.css');

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';
import { CommonsModule } from './commons/commons.module';

 export const AppModule = angular
   .module('app', [
     ComponentsModule,
     CommonsModule,
    // uiRouter
   ])
   .component('app', AppComponent)
   .config(($locationProvider) => {
    "ngInject";
    // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
    // #how-to-configure-your-server-to-work-with-html5mode
    $locationProvider.html5Mode(true).hashPrefix('!');
  })
   .name;
